#jquery-range-picker

>选择区间值的独立 jQuery 组件

##一、演示

###1、地址:<br> http://sandbox.runjs.cn/show/aphyogeq

###2、效果图:
<img src="http://git.oschina.net/bluishoul/jquery-range-picker/raw/master/resource/range-picker-demo2.png" width="648"/>

##二、如何使用
###1、引入库

    <script type="text/javascript" src="http://sandbox.runjs.cn/js/sandbox/jquery/jquery-1.7.2.min.js"></script>
    <!-- 下面两个库用来提供 drag 事件支持，也可自行替换为支持 drag 的其他库  -->
	<script type="text/javascript" src="jquery.event.drag-2.2.js"></script>
	<script type="text/javascript" src="jquery.event.drag.live-2.2.js"></script>
    <!-- jquery range picker 库  -->
	<script type="text/javascript" src="jquery.range-pikcer.js"></script>

###2、添加 Dom 节点
	<span class="daisy-range-picker"></span>

###3、启用 jquery-range-picker 插件，并配置参数
	<script type="text/javascript">
		$(function(){
			picker = $(".daisy-range-picker").range_picker({
            	//是否显示分割线
				show_seperator:false,
                //是否启用动画
			 	animate:false,
                //初始化开始区间值
				from:2,
                //初始化结束区间值
				to:5,
                //选取块宽度
				picker_width:14,
                //各区间值
				ranges:[1,2,3,4,5,6,7],
				onChange:function(from_to){
					$(".msg").html(from_to[0]+'~'+from_to[1]);
				},
				onSelect:function(index,from_to){
					$(".msg").html(from_to[0]+'~'+from_to[1]);
				},
				afterInit:function(){
					var picker = this;
					var ranges = picker.options.ranges;
					$(".msg").html(ranges[0]+'~'+ranges[ranges.length-1]);
				}
			});
		});
	</script>
##三、参数说明

`from`:开始区间值（默认值：0）    
`to` :结束区间值（默认值：∞）    
`ranges` :各区间值  （默认值：[0,1000,2000,5000,10000,'∞']）  
`axis_width` :轴长度  （默认值：200）  
`picker_width` :选取块宽度  （默认值：8）  
`show_seperator` :是否显示分割线  （默认值：true）  
`animate` :是否启用动画  （默认值：false）  
`onSelect` :当 drag 结束，选定好某个区间时调用（如下：this为当前控件实例，index:选取块所在区间序号，from_to：区间值数组）  

	onSelect:function(index,from_to){
		var self = this;
		var from = from_to[0];
		var to = from_to[1];
	}  

`beforeInit` :控件初始化前调用 （this为当前控件实例）  
`afterInit` :控件初始化后调用 （this为当前控件实例）  
`picker.options` :picker 为控件实例，`picker.options`为所有配置参数